----
--  ShotsFired
--  @Description ~%description%~
--  @Author ~%author%~
--  @Version ~%version%~
--  @Patch ~%patch%~
--

require "string";
require "table";
require "lib/lib_InterfaceOptions";
require "lib/lib_Callback2";

require "lib/lib_Debug";


--~~~~~~~~~~~~~--
-- Constants   --
--~~~~~~~~~~~~~--


--~~~~~~~~~~~~~--
-- Widgets     --
--~~~~~~~~~~~~~--
FRAME = Component.GetFrame("Main");
BURSTC = Component.GetWidget("burstc");
BURSTG = Component.GetWidget("burstg");
PERSEC = Component.GetWidget("persec");
FACTOR = Component.GetWidget("factor");

--~~~~~~~~~~~~~--
-- Variables   --
--~~~~~~~~~~~~~--
local cb_update = nil
local cb_firing = nil
local avg = 0

local shots_fired = {}
local avgt = {}
local shots_keep

local weaponState = nil
local weaponInfo = nil
--~~~~~~~~~~~~~--
-- Options     --
--~~~~~~~~~~~~~--

local ADDON = {
  NAME = "ShotsFired",
  VERSION = "~%version%~",
  AUTHOR = "~%author%~",
  LOADED = false,
  DEBUG = '~%debug%~' -- yes this is a string
}

local OPTIONS = {
  enabled = true,
  show_burstc = true,
  show_burstg = true,
  show_persec = true,
  show_factor = true,
  
  shots_time = 1000,
  shots_keep_min = 5,
  rate = 2,
  
  align = "center",
  reverse = false,
  hide = true,
  hide_delay = 5,
  
  suffix_burst = " ms",
  suffix_persec = " / s",
  suffix_factor = " %",
  
  precision_burst = 2,
  precision_persec = 2,
  precision_factor = 0,
  
  color_burstc = {
    tint = "ffffff",
    alpha = 1,
  },
  color_burstg = {
    tint = "ffffff",
    alpha = 1,
  },
  color_persec = {
    tint = "ffffff",
    alpha = 1,
  },
  color_factor = {
    tint = "ffffff",
    alpha = 1,
  },

}

local OnMessageSetData = {
    ENABLED = function(message)
      if(message) then
        Setup();
      else
        Shutdown();
      end
      OPTIONS.enabled = message
    end,
    ----------------------------------
    SHOW_BURSTC = function(message)
      OPTIONS.show_burstc = message
      SetWidgetLayout()
    end,
    SHOTS_TIME = function(message)
      OPTIONS.shots_time = message
    end,
    SHOTS_KEEP_MIN = function(message)
      OPTIONS.shots_keep_min = message
      shots_keep = message
    end,
    RATE = function(message)
      OPTIONS.rate = message
    end,
    ----------------------------------
    SHOW_BURSTG = function(message)
      OPTIONS.show_burstg = message
      SetWidgetLayout()
    end,
    SHOW_PERSEC = function(message)
      OPTIONS.show_persec = message
      SetWidgetLayout()
    end,
    SHOW_FACTOR = function(message)
      OPTIONS.show_factor = message
      SetWidgetLayout()
    end,
    ----------------------------------
    ALIGN = function(message)
      OPTIONS.align = message
      SetWidgetLayout()
    end,
    REVERSE = function(message)
      OPTIONS.reverse = message
      SetWidgetLayout()
    end,
    HIDE = function(message)
      OPTIONS.hide = message
      if(message) then
        HideFrame()
      else
        ShowFrame()
      end
    end,
    HIDE_DELAY = function(message)
      OPTIONS.hide_delay = message
    end,
    ----------------------------------
    SUFFIX_BURST = function(message)
      OPTIONS.suffix_burst = message
      SetWidgetValues()
    end,
    SUFFIX_PERSEC = function(message)
      OPTIONS.suffix_persec = message
      SetWidgetValues()
    end,
    SUFFIX_FACTOR = function(message)
      OPTIONS.suffix_factor = message
      SetWidgetValues()
    end,
    PRECISION_BURST = function(message)
      OPTIONS.precision_burst = message
      SetWidgetValues()
    end,
    PRECISION_PERSEC = function(message)
      OPTIONS.precision_persec = message
      SetWidgetValues()
    end,
    PRECISION_FACTOR = function(message)
      OPTIONS.precision_factor = message
      SetWidgetValues()
    end,
    ----------------------------------
    COLOR_BURSTC = function(message)
      OPTIONS.color_burstc = message
      SetWidgetColors()
    end,
    COLOR_BURSTG = function(message)
      OPTIONS.color_burstg = message
      SetWidgetColors()
    end,
    COLOR_PERSEC = function(message)
      OPTIONS.color_persec = message
      SetWidgetColors()
    end,
    COLOR_FACTOR = function(message)
      OPTIONS.color_factor = message
      SetWidgetColors()
    end,
}

InterfaceOptions.AddMovableFrame({
  frame = FRAME,
  label = "SF!",
  scalable = true,
});

InterfaceOptions.SaveVersion(1)
InterfaceOptions.StartGroup({id="enabled", label="Enable "..ADDON.NAME, checkbox=true, default=OPTIONS.enabled, tooltip="Disables the Addon if unchecked"});

  InterfaceOptions.AddCheckBox({id="SHOW_BURSTC", label="Show actual burst time", checkbox=true, default=OPTIONS.show_burstc, tooltip="Delay between shots in ms, calculated."});
  InterfaceOptions.AddSlider({id="SHOTS_TIME", label="Average duration", default=OPTIONS.shots_time, min=200, max=10000, inc=100, suffix=" ms", tooltip="Duration (in milliseconds) used to calculate average RoF.\nHigher values are more precise, lower values are more responsive."});
  InterfaceOptions.AddSlider({id="SHOTS_KEEP_MIN", label="Minimum shots", default=OPTIONS.shots_keep_min, min=2, max=100, inc=1, tooltip="Minimum amount of shots used to calculate average RoF.\nThis will override the setting above when needed."});
  InterfaceOptions.AddSlider({id="RATE", label="Updates per second", default=OPTIONS.rate, min=1, max=20, inc=1, tooltip="How often the UI is updated per second."});
  -- -------------------------------- 
  InterfaceOptions.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=1000, padding=12, tint="#505050"})
  -- -------------------------------- 
  InterfaceOptions.AddCheckBox({id="SHOW_BURSTG", label="Show weapon burst time", checkbox=true, default=OPTIONS.show_burstg, tooltip="Delay between shots in ms, according to the game."});
  InterfaceOptions.AddCheckBox({id="SHOW_PERSEC", label="Show shots per second", checkbox=true, default=OPTIONS.show_persec, tooltip="How many shots are fired per second. Duh."});
  InterfaceOptions.AddCheckBox({id="SHOW_FACTOR", label="Show rate of fire boost", checkbox=true, default=OPTIONS.show_factor, tooltip="Total additional rate of fire boost."});
  -- -------------------------------- 
  InterfaceOptions.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=1000, padding=12, tint="#505050"})
  -- -------------------------------- 
  InterfaceOptions.AddChoiceMenu({id="ALIGN", label="Text Alignment", default=OPTIONS.align, tooltip="Text alignment for the UI."});
    InterfaceOptions.AddChoiceEntry({menuId="ALIGN", val="left", label="left"});
    InterfaceOptions.AddChoiceEntry({menuId="ALIGN", val="center", label="center"});
    InterfaceOptions.AddChoiceEntry({menuId="ALIGN", val="right", label="right"});
  InterfaceOptions.AddCheckBox({id="REVERSE", label="Grow list upwards", checkbox=true, default=OPTIONS.reverse, tooltip="Reverse ordering and vertial orientation of the UI entries."});
  InterfaceOptions.AddCheckBox({id="HIDE", label="Hide while not firing", checkbox=true, default=OPTIONS.hide, tooltip="Hide when not firing."});
  InterfaceOptions.AddSlider({id="HIDE_DELAY", label="Hide delay", default=OPTIONS.hide_delay, min=1, max=60, inc=1, suffix=" s", tooltip="Hide UI after this many seconds of not firing."});
  -- -------------------------------- 
  InterfaceOptions.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=1000, padding=12, tint="#505050"})
  -- -------------------------------- 
  InterfaceOptions.AddTextInput({id="SUFFIX_BURST", label="Burst suffix", default=OPTIONS.suffix_burst, tooltip="Suffix for actual and weapon burst."});
  InterfaceOptions.AddTextInput({id="SUFFIX_PERSEC", label="Shots per second suffix", default=OPTIONS.suffix_persec, tooltip="Suffix for shots per second."});
  InterfaceOptions.AddTextInput({id="SUFFIX_FACTOR", label="Boost suffix", default=OPTIONS.suffix_factor, tooltip="Suffix for rate of fire boost."});
  
  InterfaceOptions.AddSlider({id="PRECISION_BURST", label="Burst precision", default=OPTIONS.precision_burst, min=0, max=4, inc=1, tooltip="Decimal precision for actual and weapon burst."});
  InterfaceOptions.AddSlider({id="PRECISION_PERSEC", label="Shots per second precision", default=OPTIONS.precision_persec, min=0, max=4, inc=1, tooltip="Decimal precision for shots per second."});
  InterfaceOptions.AddSlider({id="PRECISION_FACTOR", label="Boost precision", default=OPTIONS.precision_factor, min=0, max=4, inc=1, tooltip="Decimal precision for rate of fire boost."});
  -- -------------------------------- 
  InterfaceOptions.AddMultiArt({id="LINE", texture="CheckBox_White", region="backdrop", height=1, width=1000, padding=12, tint="#505050"})
  -- -------------------------------- 
  InterfaceOptions.AddColorPicker({id="COLOR_BURSTC", label="Actual burst color", default=OPTIONS.color_burstc});
  InterfaceOptions.AddColorPicker({id="COLOR_BURSTG", label="Weapon burst color", default=OPTIONS.color_burstg});
  InterfaceOptions.AddColorPicker({id="COLOR_PERSEC", label="Shots/s color", default=OPTIONS.color_persec});
  InterfaceOptions.AddColorPicker({id="COLOR_FACTOR", label="Boost color", default=OPTIONS.color_factor});
  -- -------------------------------- 
  
InterfaceOptions.StopGroup()
InterfaceOptions.NotifyOnLoaded(true)

function OnMessage(msg)
  if msg.type == "__LOADED" then
    ADDON.LOADED = true
    init();
  end
  if not ( OnMessageSetData[msg.type] ) then return nil end
  OnMessageSetData[msg.type](msg.data);
end

--~~~~~~~~~~~~~--
-- Collections --
--~~~~~~~~~~~~~--


--~~~~~~~~~~~~~--
-- Events      --
--~~~~~~~~~~~~~--

function OnComponentLoad() 

  log(ADDON.NAME..": --- version " .. ADDON.VERSION ..  " ---")
  local debug = (ADDON.DEBUG == '~%debug%~' or ADDON.DEBUG == 'true')
  Debug.EnableLogging(debug);
    
  InterfaceOptions.SetCallbackFunc(function(id, val)  OnMessage({type=id, data=val})  end, ADDON.NAME.." "..ADDON.VERSION)
  
end

function OnPlayerReady()
  weaponState = Player.GetWeaponState();
  weaponInfo = Player.GetWeaponInfo();
  if(OPTIONS.enabled) then
    Setup();
  end
end

function OnWeaponBurst(args)
  
  table.insert(shots_fired, tonumber(System.GetClientTime()))
  if(#shots_fired > shots_keep)then
    table.remove(shots_fired, 1)
  end
  
end


--=====================
--    Calculations   --
--=====================

function fillAvgt()
  avgt = {}
  local t = shots_fired
  
  if(#shots_fired > 1) then
    for k, v in ipairs(t) do
      local n = k
      local tts = t[n] - (t[n-1] or t[n])
      if(tts ~= 0) then
          table.insert(avgt, tts)
      end
    end
  end
end

function calcAvg()
  local sum = 0

  for key, st in ipairs(avgt) do
      sum = sum + st
  end
  
  local n = 1
  if(#avgt > 1) then
    n = #avgt
  end
  avg = sum/n
end

function cleanShots()

  local t = shots_fired
  local ts = tonumber(System.GetClientTime())
  
  local cut = 0
  
  for k, v in ipairs(t) do
    if(v < ts - calcTime()) then
      cut = k
    end
  end
  
  if(cut > 0) then
    for i = 1, cut, 1 do
      table.remove(shots_fired, 1)
    end
  end
end

function calcKeep()
  if(avg ~= 0)then
    return calcTime()/avg
  else
    return OPTIONS.shots_keep_min
  end
end

function calcTime()
  local shots_time_check = (1000/weaponInfo.RateOfFire)/weaponState.FireRateMod * OPTIONS.shots_keep_min
  if(shots_time_check > OPTIONS.shots_time) then
    return shots_time_check
  else
    return OPTIONS.shots_time
  end
end

--~~~~~~~~~~~~~--
-- Functions   --
--~~~~~~~~~~~~~--

function SetWidgetValues()
  
  shots_keep = calcKeep()
  
  fillAvgt()
  calcAvg()
  cleanShots()
  
  weaponState = Player.GetWeaponState();
  weaponInfo = Player.GetWeaponInfo();
  
  BURSTC:SetText(string.format("%."..OPTIONS.precision_burst.."f", avg) .. OPTIONS.suffix_burst)
  BURSTG:SetText(string.format("%."..OPTIONS.precision_burst.."f", (1000/weaponInfo.RateOfFire)*(weaponState.FireRateMod)) .. OPTIONS.suffix_burst)
  
  local sps = 0
  if(avg > 0)then
    sps = 1000/avg
  end
  
  PERSEC:SetText(string.format("%."..OPTIONS.precision_persec.."f", sps) .. OPTIONS.suffix_persec)
  
  local factor = 1;
  if(weaponState.FireRateMod ~= 1)then
    factor = -1
  end
  
  FACTOR:SetText(string.format("%."..OPTIONS.precision_factor.."f", factor*(weaponState.FireRateMod-1) * 100) .. OPTIONS.suffix_factor)
  
  if(cb_update)then
    cb_update:Reschedule(1/OPTIONS.rate)
  end
  if(cb_firing and avg > 0)then
    cb_firing:Reschedule(OPTIONS.hide_delay);
  end
  if(not(FRAME:IsVisible()) and avg > 0)then
    ShowFrame()
  end
end

function SetWidgetColors()
  BURSTC:SetTextColor("#"..OPTIONS.color_burstc.tint)
  BURSTC:SetParam("alpha", OPTIONS.color_burstc.alpha)
  BURSTG:SetTextColor("#"..OPTIONS.color_burstg.tint)
  BURSTG:SetParam("alpha", OPTIONS.color_burstg.alpha)
  PERSEC:SetTextColor("#"..OPTIONS.color_persec.tint)
  PERSEC:SetParam("alpha", OPTIONS.color_persec.alpha)
  FACTOR:SetTextColor("#"..OPTIONS.color_factor.tint)
  FACTOR:SetParam("alpha", OPTIONS.color_factor.alpha)
end

function SetWidgetLayout()
  
  local plus = "+"
  local pos = 70
  local r = 1
  if (OPTIONS.reverse) then
    plus = ""
    pos = -50
    r = -1
  end
  
  local positions = {}
  positions[1] = "center-y:0%"..plus..(pos-r*60)
  positions[2] = "center-y:0%"..plus..(pos-r*40)
  positions[3] = "center-y:0%"..plus..(pos-r*20)
  positions[4] = "center-y:0%"..plus..(pos-0)
  
  local frames = {
    {frame=BURSTC, show=OPTIONS.show_burstc},
    {frame=BURSTG, show=OPTIONS.show_burstg},
    {frame=PERSEC, show=OPTIONS.show_persec},
    {frame=FACTOR, show=OPTIONS.show_factor},
  }
  
  local index = 1
  for k, f in ipairs(frames) do
    if(f.show) then
      f.frame:SetDims(positions[index])
      f.frame:Show()
      index = index + 1
    else
      f.frame:Hide()
    end
  end

  BURSTC:SetAlignment("halign", OPTIONS.align)
  BURSTG:SetAlignment("halign", OPTIONS.align)
  PERSEC:SetAlignment("halign", OPTIONS.align)
  FACTOR:SetAlignment("halign", OPTIONS.align)
end

function HideFrame()
  if(OPTIONS.hide) then
    FRAME:ParamTo("alpha", 0, 1);
    FRAME:Hide(OPTIONS.hide, 1)
  end
end

function ShowFrame()
  FRAME:ParamTo("alpha", 1, 0);
  FRAME:Show()
end

--==========================
-- Rebooting, please wait --
--==========================

function Setup()
  Component.BindEvent("ON_WEAPON_BURST", "OnWeaponBurst")
  
  cb_update = Callback2.Create()
  cb_update:Bind(SetWidgetValues)
  cb_update:Reschedule(1/OPTIONS.rate);
  
  cb_firing = Callback2.Create()
  cb_firing:Bind(HideFrame)
  
  if(not(OPTIONS.enabled))then
    HideFrame()
  end
  
  if(not(OPTIONS.hide)) then
    ShowFrame()
  end
end


function Shutdown()
  Component.UnbindEvent("ON_WEAPON_BURST")
  if(cb_update ~= nil)then
    cb_update:Release();
  end
  if(cb_firing ~= nil)then
    cb_firing:Release();
  end
  cb_update = nil
  cb_firing = nil
  avg = 0
  shots_fired = {}
  avgt = {}
  callback(function()FRAME:Hide()end, nil, .1)
end




