var _ = require('underscore');

_.mixin({
  recursive: function(obj, opt, iterator) {
    function recurse(obj) {
      iterator(obj);
      for(var i = 0; i < opt.children.length; i++){
        if(obj[opt.children[i]] && obj[opt.children[i]].length > 0){
          _.each(obj[opt.children[i]], recurse);
        }
      }
    }
    recurse(obj);
  }
});

var mdbb;
module.exports = mdbb = function mdbb(config) {

    var self = this;
    this.config = config;
    
    this.result = "";
    
    this.parser = require('./parsers/'+config.parser);
    
    this._list_depth = 0;
    this._list_item = false;
    this._quote_depth = 0;
    
    this._text_actions = [
      // images
      {
        regex : /!\[(.+)\]\((.+)\)/gm,
        subst : self.parser.img.prefix + 
                self.parser.img.infix + 
                self.parser.img.suffix
      },
    
      // links
      {
        regex : /\[(.+)\]\((.+)\)/gm,
        subst : self.parser.url.prefix + 
                self.parser.url.infix + 
                self.parser.url.suffix
      },

      // inline backticks
      {
        regex : /`(.+?)`(?!.+\[\/url\])/gm,
        subst : self.parser.backticks.prefix + 
                self.parser.backticks.infix + 
                self.parser.backticks.suffix
      },
      // bold
      {
        regex : /\*{2}(\S.+?\S\*?)?\*{2}(?!.+\[\/url\]|.+\[\/img\])/gm,
        subst : self.parser.bold.prefix + 
                self.parser.bold.infix + 
                self.parser.bold.suffix
      },
      // italics
      {
        regex : /\*{1}(\S.+?\S)?\*{1}(?!.+\[\/url\]|.+\[\/img\])/gm,
        subst : self.parser.italics.prefix + 
                self.parser.italics.infix + 
                self.parser.italics.suffix
      },
      // colored - bb only, defaults to bold
      {
        regex : /\_{2}(\S.+?\S\_?)?\_{2}(?!.+\[\/url\]|.+\[\/img\])/gm,
        subst : (self.parser.colored) ?
                self.parser.colored.prefix +
                self.parser.colored.infix +
                self.parser.colored.suffix
                :
                self.parser.bold.prefix +
                self.parser.bold.infix +
                self.parser.bold.suffix
      },
      // underlined - bb only, defaults to italics
      {
        regex : /\_{1}(\S.+?\S)?\_{1}(?!.+\[\/url\]|.+\[\/img\])/gm,
        subst : (self.parser.underlined) ?
                self.parser.underlined.prefix +
                self.parser.underlined.infix +
                self.parser.underlined.suffix
                    :
                self.parser.bold.prefix +
                self.parser.bold.infix +
                self.parser.bold.suffix
      },
    ];
    
    this.processText = function(text){
      self._text_actions.forEach(function(action){
        text = text.replace(action.regex, action.subst);
      });
      return text.trim();
    };
    
    this.tags = {
      
      Heading : function(node){
        
        return self.parser.Heading.prefix +
               self.parser.Heading.option(node.depth) + 
               self.parser.Heading.infix + 
               node.text + 
               self.parser.Heading.suffix;
               
      },
      
      paragraph : function(node){
        
        return self.parser.paragraph.prefix +
               self.parser.paragraph.infix + 
               self.processText(node.text) + 
               self.parser.paragraph.suffix+
               self.parser.paragraph.option(self._quote_depth);
        
      },

      list_start : function(node){

        return self.parser.list.option(self._list_depth++) +
               self.parser.list.prefix ;
      },
      
      list_end : function(node){

        return self.parser.list.option(--self._list_depth) +
               self.parser.list.suffix;
      },
      
      list_item_start : function(node){
        self._list_item = true;
      },
      
      list_item_end : function(node){
        self._list_item = false;
      },
      
      text : function(node){
        var list_prefix = "";
        if(self._list_item && self._list_depth > 0){
          list_prefix = self.parser.list.option(self._list_depth) +
                        self.parser.list.infix;
        }

        return self.parser.text.prefix +
               self.parser.text.infix +
               list_prefix +
               self.processText(node.text) +
               self.parser.text.suffix;
      },
      
      blockquote_start : function(node){
        self._quote_depth++;
        return self.parser.blockquote.prefix;
      },
      
      blockquote_end : function(node){
        self._quote_depth--;
        return self.parser.blockquote.suffix;
      },
      
      code : function(node){
        
        return self.parser.code.prefix +
               self.parser.code.infix + 
               node.text +
               self.parser.code.suffix;
        
      }
    }
    
    this.parse = function(obj){
      if(_.has(self.tags, obj.type)){
        var out = self.tags[obj.type](obj);
        if(out){
          self.result += out;
        }
      }
    }
    
    this.convert = function(markdownMap){
      
      self.result = "";
      _.recursive(markdownMap, {children:['tokens', 'children']}, self.parse);
      return self.result;
      
    }
}